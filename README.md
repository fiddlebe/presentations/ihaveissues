# IHaveIssues

## Introduction

Business requires a new application with a "work-for-me-button". In essence, when the employee clicks the button, the application must execute all tasks that the employee normallly does, in an automated way.
That way, the employee has more free time to drink coffee.

## Specification

*  The button must be green
*  The button must display the text: Work For Me
*  When the user clicks the button, he must see a popup telling him that the application is now working for him
*  The application must determine in a dynamic way what the user does during the day, and automate those tasks
*  The application cannot request absence, premiums or do overtime
  

