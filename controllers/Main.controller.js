sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/Fragment"
    ],
	function (Controller, Fragement) {
		let Main = Controller.extend("ihaveissues.controllers.Main",
			/** @lends ihaveissues.controller.Main.prototype */
			{

            }
        );

        Main.prototype.onWorkformeStarted = function(event){
            let sourceButton = event.getSource();

            Fragement.load({
                name: "ihaveissues.fragments.progress"
            })
            .then(function(progressDialog) {
                progressDialog.openBy( sourceButton );
            });
        };

		return Main;
	}
);